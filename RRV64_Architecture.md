# ORV64 Architecture

## Architecture Overview

ORV64 is a 64-bit RISC-V Core designed for embedded applications. It has a 5 stage in-order pipeline and multi-level cache system including L1 and L2 I/D caches. ORV64 supports RV64IMAC instruction sets, Sv39 Virtual Address format, legal combinations of privilege modes in conjunction with Physical Memory Protection (PMP). It is capable of running a full-featured operating system like Linux. The core is compatible with all applicable RISC‑V standards.

ORV64 is designed to be feature a very flexible memory system that includes L1 caches, L2 caches, bus interfaces, and memory maps that provide a lot of flexibility for SoC integration.

![](assets/RRV64_Architecture/arch.jpg)

## Core Design

### Fetch

ORV64 Fetch makes up the first 2 stages of ORV64, which are the Fetch PC stage and the Pre-Decode stage.

#### Fetch PC

![](assets/RRV64_Architecture/fetch-pc.png)

ORV64 Fetch PC (FPC) is the first half of the ORV64 Fetch (IF) module, and is the first stage of ORV64’s pipeline. This block is responsible for initiating requests for instruction data, by sending PC requests to the instruction buffer (IBUF). FPC may also receive PC requests from other pipeline modules, which it will then arbitrate using a fixed priority scheme. Including FPC itself, there are 5 sources of PC requests, listed below from highest priority to lowest.

**cs2if:** Sends PC on exceptions, interrupts, trap return instructions, and when resuming from WFI mode.

**wb2if:** Sends PC when a CSR related to virtual memory or the FRM field of the fcsr has been modified. When the Pre-Decode (PD) stage of IF decodes that those CSRs will be accessed, it blocks FPC from loading the next PC until those CSRs are updated. This ensures that virtual memory translations and floating point calculations use the updated values.

**ex2if:** Sends PC on JALR and branch instructions

**ma2if:** Sends PC when completing a ORV64 Mem Access (MA) completes a fence_i. This ensures that any self modified code is flushed from ORV64 Dcache (DC) before loading a new instruction. This source doesn’t actually send a PC to load, and will only send the valid request signal. The reason for this is because the PC that would be sent by ma2if (the PC of the instruction after the fence) is still stored with FPC.

**if2if:** Sends PC for immediate jumps, the normal case (next PC=PC+4 or PC+2 for compressed instructions) and coming out of reset case. This has the same priority as ma2if, but these cases won’t collide as this PC request source will be blocked when a fence_i is decoded, and will resume after the IBUF responds with the instruction data for the ma2if PC request.

##### Interfaces

**cs2if_npc/wb2if_npc/ex2if_npc/ma2if_npc/if2if_npc:** These interfaces are used for sending PC fetch requests to FPC. This interface works using a valid/ready handshake. Excluding the valid signal, there is only 1 signal in this interface

1. pc: The PC to be fetched

**if2ic:** These interfaces are used for sending PC fetch requests from FPC to IBUF. This interface uses an enable signal to send requests. This enable signal is held high until a response is received by PD. Excluding the enable signal, there is 1 signal in this interface.

1. pc: The PC to be fetched

##### Stall Conditions

Since FPC simply arbitrates the PC requests and sends them to IBUF, the only stall condition is if FPC has a valid PC request that it’s sending to IBUF, but IBUF isn’t giving a response.

### Pre-Decode

![](assets/RRV64_Architecture/pre-decode.png)

ORV64 Pre-Decode (PD) is the second half of the ORV64 Fetch (IF) module, and is the second stage of ORV64’s pipeline. This block will send read requests to the Integer Regfile (IRF) and the Floating Point Regfile (FPRF), will determine how the if2if_npc signal is set, and contains an FSM for splitting atomic instructions into simpler instructions

#### Regfile Reads

PD will do some instruction decoding to determine if the regfiles need to be read or not. Based on what is decoded, PD may send read requests to either IRF or FPRF. These reads are synchronous, and so the regfile read data is passed directly to the next stage, in sync with the instruction.

#### IF2IF_NPC

The PD stage of IF will set the if2if_npc signals. As mentioned in the ORV64 Fetch PC page, if2if_npc is used for immediate jumps, the normal case (next PC=PC+4 or PC+2 for compressed instructions) and coming out of reset case.

#### Atomic FSM

Atomic instructions will be split into simpler instructions to reduce the hardware needed to support the RISCV atomic extension. This is accomplished using the AMO FSM in PD. The states of the FSM are as follows

1. IDLE: AMO FSM is idle
2. MEM_BAR: Send memory barrier request to L2. This ensures any pending memory accesses to L2 are completed before the atomic instruction continues to execute. This may be skipped if the atomic instruction does not have the aq/rl bits set.
3. LRSC: Execute the LR or SC instruction. 
4. MV: Store the current value of the regfile destination register. Ensures the atomic operation uses the value of the register before the load occurs, if the destination register is used as one of the atomic source registers
5. LOAD: Load the memory data
6. GAP: NOP in between the load and operation.
7. OP: Execute the atomic operation (add, and, or, xor, etc.)
8. STORE: Store the result of the atomic operation to memory.
9. DONE: End state of the AMO FSM

#### Interfaces

**if2id:** This interface contains all the data that is passed along the pipeline, from IF to ID. This interface works on a valid/ready handshake. There are 4 signals in this interface.

1. pc: PC of the instruction
2. inst: Instruction data
3. is_csr_op_on_mmu: Set if this instruction is a CSR operation that will change any of the CSRs related to virtual memory translation. Used by ID to flush the IBUF so that it will use the up to date values when doing PMP checks
4. is_rvc: Set if this instruction is an RVC instruction

**if2irf:** This interface is used by PD to send read requests to IRF. The reads are synchronous and operate using a read enable signal. Including the read enable signals, there are 4 signals in this interface.

1. rs1_addr: Read address 1
2. rs2_addr: Read address 2
3. rs1_re: Control signal. High when read to rs1_addr is valid
4. rs2_re: Control signal. High when read to rs2_addr is valid

**if2fprf:** Similar to if2irf, but used for sending read requests to FPRF. Including the read enable signals, there are 6 signals in this interface.

1. rs1_addr: Read address 1
2. rs2_addr: Read address 2
3. rs3_addr: Read address 3
4. rs1_re: Control signal. High when read to rs1_addr is valid
5. rs2_re: Control signal. High when read to rs2_addr is valid
6. rs3_re: Control signal. High when read to rs3_addr is valid

#### Stall Conditions

**wait_for_branch**: In ORV64, branches are always predicted as not taken. This causes problems for 2 specific instructions if they directly follow a branch instruction, `fence_i` and `wfe`. The problem with `fence_i` is that you don’t want to flush the Icache unnecessarily. It takes time to flush out all the entries and it will cause more compulsory cache misses. For this reason, if there are unresolved branches still left in the pipeline, and thus there is uncertainty on whether the `fence_i` should actually be executed, then PD will stall until that branch is resolved. Similarly, `wfe` poses problems as it stalls the pipeline and requires and external source to unstall it. Since this external source may not unstall the pipeline if the `wfe` instruction was never meant to be executed, PD will stall until any branches are unresolved before executing it.

### Decode

![](assets/RRV64_Architecture/decode.png)

orv64_decode (ID) is the third stage in ORV64’s pipeline. It will decode instruction data to set the control signals, receive data from the int regfile (IRF) or the floating point regfile (FPRF), read, set and clear signals in the regfile scoreboard.

#### Decode Instruction data

ID will decode the type of instruction it has, and it will use that to set the control signals that will be passed to EX. In the RTL, you can find a case statement that will call different functions depending on the instruction’s opcode, funct7 field, funct5 field, etc. These functions will output the appropriate control signals.

#### Regfile Data

The Pre-Decode stage of IF will send the read request to the regfiles (to the integer regfile or the floating point, whichever is needed). The regfile reads are synchronous, and so the regfile read data is passed directly to ID for the best performance.

#### Regfile Scoreboard

The purpose of the regfile scoreboard in ID is to track which registers still have pending writes and where that pending write will eventually come from. This is used to resolve any data hazards. There are two separate scoreboards for each regfile. Each scoreboard entry has 2 bits. 1 is to signal that there is a pending write on that regfile entry, and another bit to signal if the write will come from ORV64. When ID decodes that its instruction will eventually write to the regfile, it selects the scoreboard based on which regfile will be written to.  It indexes into the scoreboard using rd (the index of the destination register) and marks that entry and marks the source of the write (0 for ORV64). When that instruction eventually writes to the regfile, that scoreboard entry is cleared. The scoreboard helps resolve read after write data hazards, by marking which regfile data is not final and requires ID to stall or to use forwarded data, as well as write after write hazards, by ensuring that ORV64 don’t try to write to the same regfile entry.

#### Interfaces

**id2ex:** This interface contains all the data that is passed along the pipeline, from ID to EX. This interface uses a valid/ready handshake. There are 8 signals in this interface

1. pc: PC of the instruction
2. inst: Instruction data
3. rs1: Regfile data of rs1
4. rs2: Regfile data of rs2
5. rd_addr: Regfile write address
6. is_rs1_final: Should be removed since ma2ex forwarding path is not used
7. is_rs2_final: Should be removed since ma2ex forwarding path is not used
8. is_rvc: 1 if the instruction is an RVC instruction

**id2ic_fence:** This interface is used by ID to send fence requests to the Icache. This interface uses a valid/ready handshake. There is a single bit signal in this interface, but it is mostly ignored since there is only 1 type of fence sent to the Icache, and so the valid signal is all that is needed.

**id2itlb:** For the sfence.vma instruction, ID will send a flush request to the ITLB. These requests use a valid signal, which will be held high for 1 cycle. There are 3 signals in this interface

1. req_sfence_type: Contains the type of TLB flush, which are listed below
   ORV64_SFENCE_ASID: flush TLB entry if it matches the request ASID
   ORV64_SFENCE_VPN: flush TLB entry if its VPN matches the request VPN
   ORV64_SFENCE_ASID_VPN: flush TLB if its VPN and ASID matches the request VPN and ASID
   ORV64_SFENCE_ALL: flush all TLB entries
2. req_flush_asid: ASID to be checked for ORV64_SFENCE_ASID and ORV64_SFENCE_ASID_VPN flushes
3. req_flush_vpn: VPN to be checked for ORV64_SFENCE_VPN and ORV64_SFENCE_ASID_VPN flushes

**id2fp/id2mul/id2div:** These interfaces are used for passing data to the MUL, DIV and FP blocks. They’re listed as a separate interface, but they are passed along the pipeline in sync with id2ex. All of them are mostly similar, containing the input data (and rounding modes for FP) needed for calculation.

#### Stall Conditions

**wait_for_reg**: Stall for data hazard.
**wait_for_fence**: Wait for IC to accept the fence request from ID



### Execute

![](assets/RRV64_Architecture/execute.png)

orv64_execute (EX) is the fourth stage in ORV64’s pipeline. This block is responsible for calculations, sending PC requests to IF, some instruction decoding, for sending memory requests to the orv64_dcache (DC), and sending flush requests to the DTLB and VTLB.

#### Calculations

EX contains the computational components of ORV, which includes the ALU, the MUL unit, the DIV unit, and all the FP units.

**ALU:** The ALU is responsible for additions, subtractions, shifts, data comparisons (for branches and slt instructions), and bit-wise logical operations (AND, OR, XOR). The ALU is fed the operands as well as the operation type. The logic in ALU is purely combinational.

**MUL:** The MUL unit is used for multiplications. The MUL unit is fed the operands as well and the mul type. The MUL unit will begin it’s calculation on the falling edge of start_pulse. The complete output is set to 1 when MUL is done. The MUL calculation finishes in 5 cycles.

**DIV:** The DIV unit is used for division operations. The DIV unit is fed the operands as well and the division type. The DIV unit will begin it’s calculation on the falling edge of start_pulse. The complete output is set to 1 when DIV is done. DIV takes 8 cycles to complete a division operation.

**FP:** EX contains a number of floating point blocks for FP calculations. Below is the full list

1. fp_add_s: Single precision adder
2. fp_add_d: Double precision adder
3. fp_cmp_s: Single precision comparator
4. fp_cmp_d: Double precision comparator
5. fp_mac_s: Single precision multiply and accumulate
6. fp_mac_d: Double precision multiply and accumulate
7. fp_div_s: Single precision divider
8. fp_div_d: Double precision divider
9. fp_sqrt_s: Single precision square root
10. fp_sqrt_d: Double precision square root
11. fp_misc: FP Miscellaneous block. This block can convert between number types, including single to double, double to single, either floating point precision to integer, or integer to either floating point precision. This block is also used for FP sign injection and FP number classification.

The FP blocks all work in the same way. The FP units will begin their calculation on the falling edge of start_pulse. EX keeps a counter running during the FP operation, and will mark the FP operation as complete when the running counter reaches the target value. The target value depends on the FP operation, and these values can be in orv64_param_pkg.

#### PC requests

EX will send PC requests to IF on branch instructions and JALR instructions.

#### Instruction Decoding

EX has will do some instruction decoding, though not as much as ID. This decoding will set the control signals that are passed from EX to MA.

#### Memory Requests to DC

For load and store instructions, EX will send memory requests to DC. The response to this request will go to the mem_access (MA) stage. 

#### Interfaces

**ex2ma:** This interface contains all the data that is passed along the pipeline, from EX to MA. This interface works on a valid/ready handshake. There are 7 signals in this interface.

1. pc: The PC of the instruction
2. inst: The instruction data
3. ex_out: The result of EX’s calculation
4. rd_addr: The regfile write address of this instruction
5. csr_addr: Will contain the address of the CSR that will be accessed if this is a CSR instruction
6. fflags: Accrued FP exception flags
7. is_rvc: Set to 1 if this instruction is an RVC instruction

**ex2dc:** This is the interface between EX and DC, used for sending memory requests. This interface uses an enable signal, which is held high until the response is received. Including the enable signals, there are 11 signals in this interface

1. re: Read enable signal. Held high until the read response is received by MA
2. we: Write enable signal. Held high until the write response is received by MA
3. lr: Load reserved. Set to 1 when the instruction is an LR instruction
4. sc: Store conditional. Set to 1 when the instruction is an SC instruction
5. amo_store: Set to 1 if the store is an atomic store
6. amo_load: Set to 1 if the load is an atomic load
7. aq_rl: Set to 1 if the instruction is an atomic instruction with either AQ or RL fields set.
8. addr: Memory address of the request
9. mask: Write mask of the request
10. wdata: Store data
11. width: Size of the memory access. Used for PMP checking

**ex2dtlb/ex2vtlb:** For the sfence.vma instruction, EX will send flush requests to the VTLB and DTLB. These requests use a valid signal, which will be held high for 1 cycle. There are 3 signals in this interface

1. req_sfence_type: Contains the type of TLB flush, which are listed below
       ORV64_SFENCE_ASID: flush TLB entry if it matches the request ASID
       ORV64_SFENCE_VPN: flush TLB entry if its VPN matches the request VPN
       ORV64_SFENCE_ASID_VPN: flush TLB if its VPN and ASID matches the request VPN and ASID
       ORV64_SFENCE_ALL: flush all TLB entries
2. req_flush_asid: ASID to be checked for ORV64_SFENCE_ASID and ORV64_SFENCE_ASID_VPN flushes
3. req_flush_vpn: VPN to be checked for ORV64_SFENCE_VPN and ORV64_SFENCE_ASID_VPN flushes

#### Stall Conditions

**wait_for_mul:** Wait for the MUL unit to complete its calculation
**wait_for_div:** Wait for the DIV unit to complete its calculation
**wait_for_fp:** Wait for the FP calculation to finish
**hold_ex2if_kill:** Wait for IF and ID to give the ready signal after sending the kill signal to those stages on branches and JALR instructions

 

### Mem Access

![](assets/RRV64_Architecture/mem-access.png)

orv64_mem_access (MA) is the fifth stage in ORV64’s pipeline. This stage is responsible for receiving memory responses from DC, interfacing with orv64_csr (CSR), sending PC requests to IF in certain cases, writing data to both IRF and FPRF.

#### DC Responses

MA will receive memory responses from DC for load and store instructions. . Only 1 memory response is accepted per instruction. Loads will respond with the memory read data, while stores will respond with 0 data.

#### Interfacing with CSR

It is at the MA stage that CSR instructions will be able to read and write the CSRs. The reads will be asynchronous, while the writes will be synchronous. When the virtual memory translation is changed or turned off (such as for trap handling, m/s/uret instructions, or csr operations that change the virtual addressing or PMP protections) CSR may ask MA to stall and hold its current instruction, so that any blocks that use virtual memory translation are allowed to finish before these changes take effect.

#### PC Requests

As noted in the ORV Fetch page, MA is one source of PC requests. It will send these requests when a fence.i is executed.

#### IRF/FPRF Writes

It is at the MA stage that writes to both the FP and the INT regfiles will occur. Regfile writes are synchronous.

#### Interfaces

**dc2ma:** This interface is used for receiving memory responses for load and store instructions. DC will set the  valid flag for one cycle, after which the response will be lost. MA will flop and hold this response if necessary. There are 4 signals in this interface

1. valid: valid response signal. Will only be set high for 1 cycle per response
2. excp_valid: exception valid signal
3. excp_cause: The cause of the memory access exception
4. rdata: Read data

 **ma2cs/ma2cs_ctrl:** This interface is used by MA for sending read/write requests to CSR. This interface uses ma2cs_ctrl for controlling transactions to CSR. In ma2cs_ctrl, there are 3 signals

1. csr_op: CSR operation type. Will be set to CSR_OP_NONE if MA does not have a request to CSR
2. ret_type: Return instruction type (m/s/uret). Will be set to RET_TYPE_NONE if the instruction is not either of the ret type instructions mentioned
3. is_wfi: Set to 1 if the instruction is a WFI instruction

For ma2cs, there are 5 signals in this interface

1. pc: PC of the current instruction. Used mainly for exception handling
2. inst: Instruction data of the current instruction. Legacy code/Unused
3. mem_addr: Memory address of the load or store instruction. Used for updating the MTVAL CSR on load/store PMP exceptions
4. csr_addr: Request CSR address
5. csr_wdata: Write data to the CSR
6. rs1_addr: rs1 address of the instruction, or uimm field if the CSR instruction uses and immediate. Used for checking if the CSR operation should be considered a write.
7. fflags: Any floating point exception flags accrued during the floating point operation
8. is_rvc: Set if the current instruction is RVC. This is used for blocking changes to the MISA C extensions field if it would cause an alignment exception. Can be removed if the MISA extensions field is hardwired

**cs2ma:** Returns the CSR read data from CSR to MA. Since the CSR reads are asynchronous, there are no control signals in this interface, and the only signal is the read data.

**ma2if_npc_valid:** This interface is used by MA to send PC requests to IF. The request uses a valid/ready handshake and the only signal in this interface is the valid signal. Since this is only used for resuming after fences, IF will know that the PC to be loaded is the next PC after the fence.

**ma2irf/ma2fprf:** This interface is used by MA to send writes to either regfile. The write interfaces for both regfiles are the same. Writes will be validated using an active high write enable signal. Including the enable signal, there are 3 signals in this interface

1. rd: Write data
2. rd_addr: Regfile write address
3. rd_we: Write enable

#### Stall Conditions

**wait_for_dc:** MA is waiting for the memory response from LSU so it can commit the current instruction

**cs2ma_stall:** As mentioned above, CS may ask MA to stall so that any blocks using the current virtual address translation signals are allowed to finish



### Write Back

ORV64 Write Back (WB) is the sixth and final stage in ORV64’s pipeline. There is very little logic in this stage, and the RTL for this stage is contained within orv64_mem_access. The only function of this stage is to send PC requests to IF after any change to any CSR that affects virtual memory translation or PMPs has taken effect.

#### Interfaces

**wb2if_npc:** This function of this interface is described above and operates on a valid/ready handshake. There are 2 signals in this interface

1. valid: Signals that this PC request is valid
2. pc: Request PC



## Advanced Features Design

### Instruction Buffer

The instruction buffer is mainly used to prefetch instructions from L1 Cache. In addition to the instruction requested by the IF, the instruction buffer also fetches the instructions of the next two cache lines. If the execution flow is sequential, or there is a forward jump whose span is less than two cache lines, the instruction buffer will hit and return the instruction data within one cycle since we have already fetch it before. When a branch or jump instruction is taken and the instruction corresponding to the destination address is not currently in instruction buffer, the instruction buffer will be flushed and send a request to ICache.

### Loop Buffer

Loop buffer is a high speed D-Cache type memory that is used for holding up to 64 of the most recently fetched instructions. It is maintained by the IF stage of the pipeline. If a branch instruction is taken, we can first check the loop buffer to see if the instruction exists. If the loop buffer hits, the instruction data will be returned to IF within a cycle. If not, the loop buffer will wait for the instruction data be fetched from instruction buffer or L1 Cache and use this instruction to replace the oldest instruction in loop buffer.

### Address Translation

To support an operating system, ORV64 features full hardware support for address translation via a Memory Management Unit (MMU). It has separate configurable data and instruction TLBs. The TLBs are fully set-associative memories. On each instruction and data access, they are checked for a valid address translation. If none exists, ORV64’s hardware PTW queries the main memory for a valid address translation. The replacement strategy of TLB entries is Pseudo Least Recently Used (LRU).

Both instruction cache and data cache are virtually indexed and physically tagged and fully parametrizable. The address is split into page offset (lower 12 bit) and virtual page number (bit 12 up to 39). The page offset is used to index into the cache while the virtual page number is simultaneously used for address translation through the TLB. In case of a TLB miss the pipeline is stalled until the translation is valid.

### Exception Handling

Exceptions can occur throughout the pipeline and are hence linked to a particular instruction. The first exception can occur during instruction fetch when the PTW detects an illegal TLB entry or the address is not aligned. During decoding, exceptions can occur when the decoder detects an illegal instruction. As soon as an exception has occurred, the corresponding instruction is marked and auxiliary information is saved. Such excepting instruction will be handled by the exception handler at the MA stage.

Interrupts are asynchronous exceptions, in ORV64, they are synchronized to a particular instruction. Like exception, the interrupt signal will be processed in the MA stage.

### Privileged Extensions

The privileged specification defines more CSRs governing the execution mode of the hart. The base supervisor ISA defines an additional interrupt stack for supervisor mode interrupts as well as a restricted view of machine mode CSRs. Accesses to these registers are restricted to the same or a higher privilege level.

CSR accesses are executed in the MA stage. Furthermore, a CSR access can have side-effects on subsequent instructions which are already in the pipeline e.g. altering the address translation infrastructure. This makes it necessary to completely flush the pipeline on such accesses.



## Cache Design

### Cache overview

The ORV64 core is equipped with private L1 instruction and unified L2 cache. The overall design of our internal memory hierarchy is illustrated in following block diagram.

#### Multi-core

![](assets/RRV64_Architecture/internal_memory_blockdiagram3.png)

### L1 Cache

#### L1 Instruction Cache

As part of memory hierarchy, the L1 instruction cache helps cut down the latency of cpu instruction fetching.

The parameter of L1 instruction cache is as follows:

> | Cache capacity | Cache line numbers | Cache line capacity | Mapping method        |
> | -------------- | ------------------ | ------------------- | --------------------- |
> | 8 KBytes       | 128                | 32 Bytes            | 2-way set associative |



### L2 Cache

#### Overview

The L2 cache is a 256KB, 4-bank, 4-way set associative shared L2 cache. The latency of L2 cache is 4 cycles at hit. The L2 cache RAM reading and writing processes are pipelined into 4 stages for less RAM access and higher frequency. The L2 cache is designed as a non-blocking cache which can handle hit-under-miss and miss-under-miss using the Missing Status Holding Registers (MSHRs). With non-blocking L2 cache design, memory system can execute out-of-order and more latency can be hidden.

![](assets/RRV64_Architecture/l2_1.png)

#### Parameter

The parameter of L1 data cache is as follows:

> | Cache capacity | Cache line numbers | Cache line capacity | Mapping method        |
> | -------------- | ------------------ | ------------------- | --------------------- |
> | 256 KBytes     | 512                | 32 Bytes            | 4-way set associative |

#### L2 cache pipeline

The L2 is designed as 4-stage-pipeline for low power and high frequency. In the first 3 stages, valid, tag, lru, dirty and data RAMs are serially checked, which means some of the RAMs are not needed to be accessed if the information got from previous stages tells the control logic not to.

The Missing Status Holding Registers lie in the stage 4, which has the ability to hold multiple cache missed request to the next level memory, without blocking the whole pipeline. This is a key feature for Out-of-Order memory system.

![](assets/RRV64_Architecture/l2_2.png)
