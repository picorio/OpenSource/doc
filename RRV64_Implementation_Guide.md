# RRV64 Implementation User's Guide

## Synthesis Environment

The environment is under $PROJECT_ROOT/hardware/soc/syn/dc. 

It is based on Synopsy Design Compiler. 

![image-20210918130207570](assets/RRV64_Implementation_Guide/image-20210918130207570.png)

There are several folders in the synthesis root for different synthesis inputs and outputs

- *log* contains run log of design compile
- *output* contains some intermediate outputs during synthesis like .ddc, .sdc
- *rpt* contains the synthesis reports
- *run* is the path where the synthesis is running 
- *script* contains the synthesis scripts and constraints

## Synthesis Settings

The target node is TSMC28, target frequency is 400MHz. There RRV64 works under single clock.

![image-20210918131233280](assets/RRV64_Implementation_Guide/image-20210918131233280-16323921000102.png)

There are multi-cycle paths for FPU/DIV which is also specified in the sdc

![image-20210918131431580](assets/RRV64_Implementation_Guide/image-20210918131431580.png)

## Run Synthesis 

To run synthesis, firstly set up the environment variables at project root. Then switch to the synthesis directory and run

```
source $PROJECT_ROOT/build/env.sh

cd $PROJECT_ROOT/hardware/soc/syn/dc

make clean

make t28

```



