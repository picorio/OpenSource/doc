# RVV64 Integration Guide

## Overview

### General Description

RVV64 is a 5-stage pipelined RISC-V core. It interacts with the system through AXI4 interfaces and in-house designed high-speed L2 bank memory-facing interface. It has both AXI master and slave interfaces, master interfaces is needed only when core has to access uncachable memory or memory map IO sapce, slave interfaces is for access core internal resource, for example it can be used to read itb or tlb content. L2 bank interface Memory interface is designed to achieve high-speed communication with system memory. The core also has some signals specifically for functionality configuration and debug information, such as icache bypass, itb enable, etc., users can connect registers to these signals in order to adapt core configurations according to the application and turn on debug capabilities if necessary. The core can take 4 types of interrupt, including software interrupt, external interrupt, timer interrupt and external event.

### Block Diagram

![](assets/RRV64_Integration_Guide/Integration-guide-block-diagram.png)

## Top Level Integration

### Signal Discriptions

This chapter details all possible I/O signals in the core.

#### System Clock, Resets

![](assets/RRV64_Integration_Guide/Integration-guide-sysclk-reset.png)

| **Port Name** | **I/O** | **Description**                                              |
| :------------ | :------ | :----------------------------------------------------------- |
| clk           | I       | RRV64 clock, all logic runs on this clock.                   |
| s2b_rst       | I       | Active high reset signal.0 - Takes core out of reset1 - Resets the core |
| s2b_early_rst | I       | Active high early reset signal, controls debug access logic. |

#### Core ID

![](assets/RRV64_Integration_Guide/Integration-guide-core-id.png)

| **Port Name** | **I/O** | **Description**  |
| :------------ | :------ | :--------------- |
| core_id[7:0]  | I       | Specify core id. |

#### Configuration & Debug Signals (for n = 0, n < 4)

![](assets/RRV64_Integration_Guide/Integration-guide-cfg-dubug-signal.png)

| **Port Name**           | **I/O** | **Description**                                              |
| :---------------------- | :------ | :----------------------------------------------------------- |
| s2b_en_bp_if_pc_n       | I       | Enable breakpoint n for a specific IF pc.                    |
| s2b_bp_if_pc_n[38:0]    | I       | IF pc value to set breakpoint n.                             |
| s2b_en_bp_wb_pc_n       | I       | Enable breakpoint n for a specific WB pc.                    |
| s2b_bp_wb_pc_n[38:0]    | I       | WB pc value to set breakpoint n.                             |
| s2b_en_bp_instret       | I       | Enable breakpoint for a specific instruction return count.   |
| s2b_bp_instret[63:0]    | I       | Instruction return count for breakpoint.                     |
| s2b_cfg_itb_en          | I       | Enable instruction trace buffer.                             |
| s2b_cfg_itb_sel[2:0]    | I       | Mode for ITB log.0: log IF pc1: log ID pc2: log EX pc3: log MA pc4: log WB pc |
| s2b_cfg_itb_wrap_around | I       | Enable wrap around for ITB log.Since ITB has limited entries, once itb reaches the last entry it will stop logging unless this wrap aournd feature is enabled.If wrap around feature is enabled, it will keep logging the latest pc, which means old log will be overwritten if all entries are occupied. |
| b2s_itb_last_ptr[4:0]   | O       | Indicating latest ITB entry.                                 |
| s2b_debug_stall         | I       | Stall the core.                                              |
| s2b_debug_resume        | O       | Resume the core.                                             |
| b2s_debug_stall_out     | O       | Core debug stall status.After setting s2b_debug_stall to 1, usually needs to pull this bit to be 1 in order to make sure debug stall is set successfully |
| s2b_cfg_rst_pc[38:0]    | I       | Core reset pc. After core reset is released, it will fetch the first instrction from this pc. |
| s2b_cfg_pwr_on          | I       | Core Power on.                                               |
| s2b_cfg_sleep           | I       | Core sleep.                                                  |
| s2b_cfg_lfsr_seed[5:0]  | I       | Random seed for icache replacement.                          |
| s2b_cfg_bypass_ic       | I       | Enable icache bypass.                                        |
| s2b_cfg_bypass_tlb      | I       | Enable tlb bypass.                                           |
| s2b_cfg_en_hpmcounter   | I       | Enable performance counter.                                  |
| b2s_if_pc               | O       | Indication current IF pc.                                    |
| wfi_stall               | O       | Core is in wfi.                                              |
| wfe_stall               | O       | Core is in wfe.                                              |

#### AXI Slave Interface Signals

![](assets/RRV64_Integration_Guide/Integration-guide-axi-slave-interface-signal.png)

| **Port Name**        | **I/O** | **Description**                                              |
| :------------------- | :------ | :----------------------------------------------------------- |
| ring_req_if_awvalid  | I       | AXI write address valid.                                     |
| ring_req_if_aw[51:0] | I       | AXI write address information.[51:40] 12-bit awid[39:0] 40-bit awaddr |
| ring_req_if_awready  | O       | AXI write address ready.                                     |
| ring_req_if_wvalid   | I       | AXI write data valid.                                        |
| ring_req_if_w[72:0]  | I       | AXI write data information.[72: 9] 64-bit wdata[8:1] 8-bit wstrb[0] - 1-bit wlast |
| ring_req_if_wready   | O       | AXI write data ready.                                        |
| ring_req_if_arvalid  | I       | AXI read address valid.                                      |
| ring_req_if_ar[51:0] | I       | AXI read address information.[51:40] 12-bit arid[39:0] 40-bit araddr |
| ring_req_if_arready  | O       | AXI read address ready.                                      |
| ring_resp_if_bvalid  | O       | AXI write response valid.                                    |
| ring_resp_if_b[13:0] | O       | AXI write response information.[13:2] 12-bit bid[1:0] 2-bit bresp |
| ring_resp_if_bready  | I       | AXI write response ready.                                    |
| ring_resp_if_rvalid  | O       | AXI read response valid.                                     |
| ring_resp_if_r[77:0] | O       | AXI read response information.[77:14] 64-bit rresp[13:2] 12-bit rid[1:0] 2-bit rresp |
| ring_resp_if_rready  | I       | AXI read response ready.                                     |

#### AXI Master Interface Signals

![](assets/RRV64_Integration_Guide/Integration-guide-axi-maser-interface-signal.png)

| **Port Name**          | **I/O** | **Description**                                              |
| :--------------------- | :------ | :----------------------------------------------------------- |
| sysbus_req_if_awvalid  | O       | AXI write address valid.                                     |
| sysbus_req_if_aw[51:0] | O       | AXI write address information.[51:40] 12-bit awid[39:0] 40-bit awaddr |
| sysbus_req_if_awready  | I       | AXI write address ready.                                     |
| sysbus_req_if_wvalid   | O       | AXI write data valid.                                        |
| sysbus_req_if_w[72:0]  | O       | AXI write data information.[72: 9] 64-bit wdata[8:1] 8-bit wstrb[0] - 1-bit wlast |
| sysbus_req_if_wready   | I       | AXI write data ready.                                        |
| sysbus_req_if_arvalid  | O       | AXI read address valid.                                      |
| sysbus_req_if_ar[51:0] | O       | AXI read address information.[51:40] 12-bit arid[39:0] 40-bit araddr |
| sysbus_req_if_arready  | I       | AXI read address ready.                                      |
| sysbus_resp_if_bvalid  | I       | AXI write response valid.                                    |
| sysbus_resp_if_b[13:0] | I       | AXI write response information.[13:2] 12-bit bid[1:0] 2-bit bresp |
| sysbus_resp_if_bready  | O       | AXI write response ready.                                    |
| sysbus_resp_if_rvalid  | I       | AXI read response valid.                                     |
| sysbus_resp_if_r[77:0] | I       | AXI read response information.[77:14] 64-bit rresp[13:2] 12-bit rid[1:0] 2-bit rresp |
| sysbus_resp_if_rready  | O       | AXI read response ready.                                     |

#### System L2 bank Memory Interface

![](assets/RRV64_Integration_Guide/Integration-guide-sys-mem-interface.png)

| **Port Name**  | **I/O** | **Description**                                              |
| :------------- | :------ | :----------------------------------------------------------- |
| l2_req_valid   | O       | System memory request valid.                                 |
| l2_req[342:0]  | O       | System memory request information.[342:303] 40-bit request address[302: 47] 256-bit request data[46:36] 11-bit request id[35:4] 32-bit request mask[3:0] 4-bit request type |
| l2_req_ready   | I       | System memory request ready.                                 |
| l2_resp_valid  | I       | System memory response valid.                                |
| l2_resp[298:0] | I       | System memory response information.[298:43] 256-bit response data[42: 11] 32-bit response mask[10:0] 11-bit response id |
| l2_resp_ready  | O       | System memory response ready.                                |

#### Interrupt

![](assets/RRV64_Integration_Guide/Integration-guide-interrupt.png)

| **Port Name** | **I/O** | **Description**     |
| :------------ | :------ | :------------------ |
| ext_int       | I       | External interrupt. |
| sw_int        | I       | Software interrupt. |
| timer_int     | I       | Timer interrupt.    |
| s2b_ext_event | I       | Externel Event.     |

### Integration Considerations

#### Clock Domain

As mentioned before, all logic inside RVV64 works under a single clock domain. If any modules works under a different clock domain interacts with the core, CDC needs to carefully handled outside.

#### Power Domain

The entire RVV64 works under a singal power domain except icache SRAM. For nornal operation, s2b_cfg_pwr_on needs to be 1. If s2b_cfg_pwr_on is 0, the SRAM will be shutdown.



## ASIC DC Synthesis

In the soc/syn/dc directory, execute the make default command or the make t28 command.

```
source build/env.sh
cd hardware/soc/syn/dc
make default 
```

It will synthesize with synopsys dc's own teaching library.

or 

```
make t28
```


It will synthesize with the tsmc 28nm process library.

The synthesis result is stored in the build directory of syn directory, and the synthesis report is stored in the syn/build/rpt directory.

*Note: Before you run DC synthesis, please make sure the t28 library is avaliable.*

## Power analyze

```
cd hardware/soc/pa
./HYDRA.run
```
The current power analyze tools is PowerArtist. Tech lib is GF22nm. The report resaults will be generated to rpt directory.
The rrv64 core's clock is 500MHz.



## FPGA Prototyping

### Generate Bitfile

The FPGA synthesis command has been wrapped and only needs to execute the following command.

```
make generate-bitfile
```

This command automatically invokes the vivado synthesis flow: create project -> synth -> impliment -> generate bitfile.  The output bitfile will be in the directory hardware/soc/bitfile.

This command actually calls the make command in the following path. If you need to run a phase of the flow separately, you can go to that directory and execute the corresponding make command separately.

```
cd hardware/soc/tool/driver/es1y
```

![image-20210913181229415](assets/RRV64_Integration_Guide/image-20210913181229415.png)

*Note: Before you run FPGA synthesis, please make sure the FPGA license is avaliable.*

![image-20210913181751035](assets/RRV64_Integration_Guide/image-20210913181751035.png)

Click help-> manage licese, and then copy your own license.

### Program Bitfile Into FPGA

Before you interactive with the FPGA, make sure the related FPGA tools (fesvr and driver) are avaliable. One can build driver and fesvr tool easily in the top directory of the project.

```
make pk
make bbl
make fesvr
make driver
```

Then you can program bitfile as below:

```
make connect
```

### Boot Linux

Go to the top directory of the project,  boot linux as below:

```
make linux
```

More detail command execution can be found in the top Makefile of the project package. 
