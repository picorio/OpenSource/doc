# 1 introduction

gem5 is a discrete event driven open-source simulation platform supports multiple ISAs including RISCV. With modulated design, gem5's components can be rearranged, parameterized, extended or replaced to simulate different micro architectures, for use in architecture exploration.  

gem5 provides several modes adapt to different needs range from fast running to accurate timing simulation. In general it runs faster than RTL simulation and more accurate than pure instruction level simulation like spike/qemu.



![simulator trade off](assets/RRV64_Performance_Model_User_Guide/simulator_trade_off.png)





gem5 is written primarily in C++ and python with most components provided under a BSD style license. It can simulate a complete system with devices and an operating system  in full system mode (FS mode), or user space only programs where system  services are provided directly by the simulator in syscall emulation  mode (SE mode). 



## 1.1 official docs, tutorials and other resources

official site:

https://www.gem5.org/

documentations: 

http://learning.gem5.org/

https://www.gem5.org/documentation/

book:

http://learning.gem5.org/book/index.html

source:

https://github.com/powerjg/learning_gem5

video course:

https://www.youtube.com/channel/UCBDXDuN_5XcmntoE-dnQPbQ







# 2 running gem5

## 2.1 install and build

Getting code and install dependencies: see 

http://learning.gem5.org/book/part1/building.html

(they are ready in repo)

build: in gem5 root folder:

```
scons build/RISCV/gem5.opt -j($nproc)
```

"scons" is SConstruct, a python based software construction tool. gem5 use is to build.

"gem5.opt" is the main binary file to run. The second part of the path "RISCV" represent ISA used. It could be replaced by any ISA gem5 supported, like X86 or ARM. We use RISCV here only.

## 2.2 run gem5 scripts

To run gem5, a python script is needed. it runs like this:

```
build/RISCV/gem5.opt [main arguments] script_file.py [script arguments]
```

For a hello world example: 

```
build/RISCV/gem5.opt configs/example/se.py -c tests/test-progs/hello/bin/riscv/linux/hello
```

The file "hello" is a prebuilt binary elf file, "-c" or "--cmd=" designated it to the script file as a workload.

It's a bit confusing at the beginning that "gem5.opt" and the [script].py file have there own command line parameters. To see parameters of gem5.opt, type:

```
build/RISCV/gem5.opt --help
```

To see parameters of your script file, type "--help" after your python script file, like:

```
build/RISCV/gem5.opt configs/example/se.py --help
```

Gem5 uses python to configure arguments, writing them into configuration file `m5out/config.ini`, then start C++ code and read arguments from it. So basically python scripts does the configuration and initialization work. You can manipulate the components gem5 provided (especially peripheral devices outside CPU like buses and caches), combine them into a system, and run with certain workloads. But CPU architectures must be modified in C++ code.

## 2.3 System Emulation (SE) mode and Full System mode (FS) in gem5

gem5 provides 2 different mode, System Emulation (SE) mode and Full System (FS) mode.

In SE mode: 

* runs user-mode binaries
* Decode catches syscalls
* emulates in src/sim/syscall_emul

In FS mode:

* Runs unmodified OS

* Emulates or models all devices

* support some ISAs better than others

  

### 2.3.1 run elf in SE mode

A simple example to run in SE model would be using config/example/fs.py. In FS model, gem5 handles system call. Compile source file with riscv64-unknown-elf-gcc with no modification would be OK. Establish a source file hello.c like below:

```
// filename : hello.c
#include <stdio.h>

int main(int argc, char* argv[])
{
    printf("Hello world! Hello riscv!\n");
    return 0;
}
```

Compile:

```
 riscv64-unknown-elf-gcc hello.c
```

run:

```
build/RISCV/gem5.opt configs/example/se.py -c a.out 
```

gem5 then give output:

```
**** REAL SIMULATION ****
build/RISCV/sim/simulate.cc:107: info: Entering event queue @ 0.  Starting simulation...
Hello world! Hello riscv!
Exiting @ tick 1113000 because exiting with last active thread context
```

Note that gem5 now only support riscv 64 bit instructions.

There are several arguments provided by script se.py, see them by:

```
build/RISCV/gem5.opt configs/example/se.py --help
```



### 2.3.2 run linux image in FS mode

to run bbl (berkerly boot loader) incorporated with a linux image, run:

```
build/RISCV/gem5.opt --debug-flags=Exec -d m5out -re configs/example/riscv/fs_linux.py --kernel=/home/cs/workspace/simulator/riscv/out/bbl_single --caches --mem-size=256MB --mem-type=DDR4_2400_8x8 --cpu-type=AtomicSimpleCPU --disk-image=$OUT/riscv_disk -n 1
```

argument -r redirects stdout to file simout, -e redirects stderr to simerr,  the defualt path is gem5/m5out, it could be set by "-d [log path]"

arguments --debug-flags=[flags] selects debug logs to output

in FS mode, target console is redirected to a simulated console, to use it:

``` 
cd util/term
make
```

and then:

```
./m5term local host 3456
```

note that **m5term** is a client program that can only run correctly after gem5.opt with FS script and a correct linux workload is stated, otherwise **m5term** will terminate with no other effects.

If **m5term** started correctly, it will show:

```
==== m5 terminal: Terminal 0 ====
```

reference: https://github.com/ppeetteerrs/gem5-RISC-V-FS-Linux



### 2.3.3 run gem5 in interactive mode

Gem5 employs python to configure arguments, but if type those lines in scripts, you will get errors.

First of all,  an interactive console provided by PyPy **ipython** is stronly recommended, it's a powerful tool to test and debug python code, you can install in by `pip3 install ipython`

Let's start ipython

```
> ipython
Python 3.8.5 (default, Jul 28 2020, 12:59:40)
Type 'copyright', 'credits' or 'license' for more information
IPython 7.19.0 -- An enhanced Interactive Python. Type '?' for help.

In [1]:
```

Then type some content from gem5's python scripts, like:

```
In [1]: import m5
```

You will get an error message:

```
In [1]: import m5
---------------------------------------------------------------------------
ModuleNotFoundError                       Traceback (most recent call last)
<ipython-input-1-77cb7d6ae89f> in <module>
----> 1 import m5

ModuleNotFoundError: No module named 'm5'
```

That's because, the main binary file build/XXX/gem5.opt handles the python packge issue. To test code in interactive code, we could use parameter `--interactive` or` -i` for short.

Still, gem5 requires a *.py script to start with, even an empty script will be OK. So let's just establish en empy file:

```
touch configs/example/empty.py
```

Then run empty file in interactive mode:

```
build/RISCV/gem5.opt -i configs/explore/empty.py
```

Here we can enter interactive mode correctly (gem5 will try to start ipython, if not installed, a default python console with be started)

```
gem5 Interactive Console
In [1]: import m5

In [2]: m5
Out[2]: <module 'm5' (<importer.CodeImporter object at 0x7f54fc32e730>)>
```

If the script file is not empty, gem5 will start interactive mode with all the content in the script executed

```
build/RISCV/gem5.opt -i configs/example/se.py -c a.out
**** REAL SIMULATION ****
build/RISCV/sim/simulate.cc:107: info: Entering event queue @ 0.  Starting simulation...
Hello world! Hello riscv!
Exiting @ tick 1113000 because exiting with last active thread context
----/python3.8/site-packages/IPython/config.py:12: ShimWarning: The `IPython.config` package has been deprecated since IPython 4.0. You should import from traitlets.config instead.
  warn("The `IPython.config` package has been deprecated since IPython 4.0. "
/home/cs/.local/lib/python3.8/site-packages/IPython/core/interactiveshell.py:632: UserWarning: As of IPython 5.0 `PromptManager` config will have no effect and has been replaced by TerminalInteractiveShell.prompts_class
  warn('As of IPython 5.0 `PromptManager` config will have no effect'
gem5 Interactive Console
In [1]:
```

This will make script debugging convient.



# 3 understanding gem5

As illustrated above, gem5 use python to do configuration. In python part, all configuration would be parsed, then write into a config file `m5out/config.ini`. Then in C++ code, simulation system would be initiallized according to this file.The file look like this:

```
[root]
type=Root
children=system
eventq_index=0
full_system=true
sim_quantum=0
time_sync_enable=false
time_sync_period=100000000000
time_sync_spin_threshold=100000000

[system]
type=System
children=bridge clk_domain cpu cpu_clk_domain cpu_voltage_domain dvfs_handler iobus iocache mem_ctrls membus platform voltage_domain workload
byte_order=little
cache_line_size=64
eventq_index=0
exit_on_work_items=false
init_param=0
m5ops_base=0
……
```

You can check this file to see initialization configs. Meanwhile, some script file in gem5 provides several arguments including memory, cache configuration, etc, . Check them by --help after python script file.



## 3.1 manipulate system configuration in python scripts

### 3.1.1 model an atomic simple cpu 

Let's get a simple script file to see what gem5 does in it.

Make a new file:

```
touch configs/example/simple_cpu.py
vim configs/example/simple_cpu.py
```

First, let's import libraries:

```
# import the m5 (gem5) library created when gem5 is built
import m5
# import all of the SimObjects
from m5.objects import *
```

System represents the simulated hardware platform, holding basic features like frequency.

```
system = System()

# Set the clock fequency of the system (and all of its children)
system.clk_domain = SrcClockDomain()
system.clk_domain.clock = '1GHz'
system.clk_domain.voltage_domain = VoltageDomain()

# Set up the system
system.mem_mode = 'timing'               # Use timing accesses
system.mem_ranges = [AddrRange('512MB')] # Create an address range
```

Setting up a CPU object:

Gem5 provides several cpu types, see them by 

```
build/RISCV/gem5.opt -i configs/example/se.py --list-cpu-types 
```

Here we choose TimingSimpleCPU in our test, it employs simple timing about memory, no timing within CPU.

```
system.cpu = TimingSimpleCPU()
```

Then we make a memory bus:

```
system.membus = SystemXBar()
```

cpu, memory, caches are connected through Ports, which is an important concept in gem5. Before, there are "master_port" and "slave_port", in latest version, gem5 has switched to "cpu_side_ports" and "mem_side_ports", which looks more clear. But there are some old script files haven't switched yet, you may see some warnings about it.

Let's connect them:

```
system.cpu.icache_port = system.membus.cpu_side_ports
system.cpu.dcache_port = system.membus.cpu_side_ports

system.cpu.createInterruptController()

system.mem_ctrl = MemCtrl()
system.mem_ctrl.dram = DDR3_1600_8x8()
system.mem_ctrl.dram.range = system.mem_ranges[0]
system.mem_ctrl.port = system.membus.mem_side_ports

# Connect the system up to the membus
system.system_port = system.membus.cpu_side_ports
```

Here we connect cpu directly to membus, with no caches. How ever caches could be inserted between them easily.

After setting up system, cpu, caches, buses and memory, we should set up work load:

```
# path to elf file, this is the helloworld file
# we just created in 2.3.1
binary = "a.out" 

system.workload = SEWorkload.init_compatible(binary)
```

Other init works:

```
process = Process()
process.cmd = [binary]
system.cpu.workload = process
system.cpu.createThreads()

root = Root(full_system = False, system = system)
```

Final initiallization and start:

```
m5.instantiate()

print("========== Beginning simulation! ")
exit_event = m5.simulate()
print('("========== Exiting @ tick {} because {}'
      .format(m5.curTick(), exit_event.getCause()))
```

Then we can try run it:

```
build/RISCV/gem5.opt configs/example/simple_cpu.py
```





## 3.2 modifying and extending modules

### 3.2.1 simple case to create a gem5 SimObject class

* Step 1: create a python class 

```
# HelloObject.py
from m5.params import *
from m5.SimObject import SimObject

class HelloObject(SimObject):
    type = ‘HelloObject’
    cxx_header = ‘learning_gem5/hello_object.hh’ 
```

here:

"type": the C++ class name

"m5.params": things like MemorySize, Int, etc. basically, it wrapped every thing you want to pass from python script to C++

"cxx_header": The filename for the C++ header file



* Step 2: Implement the C++

```
// hello_object.hh
#include "params/HelloObject.hh"
#include "sim/sim_object.hh"
class HelloObject : public SimObject
{
  public:
    HelloObject(HelloObjectParams *p);
};
```

param/*.hh generated automatically. Comes from Python SimObject definition

Constructor has one parameter, the generated params object.



```
// hello_object.cc
HelloObject::HelloObject(HelloObjectParams *params)
: SimObject(params)
{
std::cout << "Hello World! From a SimObject!" << std::endl;
}
HelloObject*
HelloObjectParams::create()
{
return new HelloObject(this);
}
```

HelloObjectParams: when you specify a Param in the Hello.py file, it will be a member of this object.

You must define the "create()" function (you’ll get a linker error otherwise). This is how Python config creates the C++ object.



* Step 3: Register the SimObject and C++ file

```
# SConscript

# SConscript is just Python
Import(*)

# Indicates that this Python file contains a SimObject. 
# Note: you can put pretty much any python here
SimObject(‘Hello.py’)

# Tell scons to compile this file
Source(‘hello_object.cc’)
```





* Step 4: (Re-)build gem5

```
scons build/RISCV/gem5.opt -j$(nproc)
```



* Step 5: Create a config script

```
# configs/learning_gem5/hello.py

# Instantiate the new object that 
# you created in the config file
...
system.hello = HelloObject()
...
```

Then run it:

```
build/X86/gem5.opt configs/learning_gem5/hello.py

...
Hello world! From a SimObject!
...
```



See example files below:

gem5/src/learning_gem5/part2/hello_object.cc
gem5/src/learning_gem5/part2/hello_object.hh
gem5/src/learning_gem5/part2/HelloObject.py
gem5/configs/learning_gem5/part2/hello_run.py



## 3.3 add new CPU core



# 4 performance output

gem5 could output performance log, ensuring visulalization of O3 CPU models

when running, add gem5.opt arguments --debug-flags=O3PipeView

```
 ./build/ARM/gem5.opt --debug-flags=O3PipeView --debug-start= --debug-file=trace.out configs/example/se.py --cpu-type=detailed --caches -c -m
```

A file "trace.out" with be generated in the log folder (default gem5/m5out). It looks like:

```
... ...
O3PipeView:decode:1171000
O3PipeView:rename:1172000
O3PipeView:dispatch:1174000
O3PipeView:issue:1175000
O3PipeView:complete:1176000
O3PipeView:retire:0:store:0
O3PipeView:fetch:1180000:0x800002b4:0:186:addi ra, zero, 13
O3PipeView:decode:1181000
O3PipeView:rename:1182000
O3PipeView:dispatch:1184000
O3PipeView:issue:1184000
O3PipeView:complete:1185000
O3PipeView:retire:1187000:store:0
O3PipeView:fetch:1180000:0x800002b8:0:187:addi a4, ra, 11
... ...

```

will can generate a visualization from it

![simulator trade off](assets/RRV64_Performance_Model_User_Guide/perf_figure_1.png)





