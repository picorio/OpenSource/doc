# RRV64 SDK User's Guide



## Introduction to RRV64 SDK

The software development kit mainly includes riscv-toolchain, linux kernel, boot loader, file system, benchmark test and tec. The following file structure is the project repo. The SDK components can be found in the following software subdirectory of the project repo and the excutable files are under the output subdirectory after building.

```

.
├── build
│   ├── Makefile
│   └── README.md
├── doc
│   ├── assets
│   ├── README.md
│   ├── RRV64_Architecture.md
│   ├── RRV64_Implementation_Guide.md
│   ├── RRV64_Integration_Guide.md
│   ├── RRV64_Performance_Model_User_Guide.md
│   ├── RRV64_Processor_Databook.md
│   ├── RRV64_Sail_User_Guide.md
│   ├── RRV64_SDK_User_Guide.md
│   ├── RRV64_Verification_User_Manual.md
│   ├── Spike_User_Guide.md
│   └── Talon_User_Guide.md
├── hardware
│   ├── core
│   └── tool
├── Makefile -> build/Makefile
├── output
│   ├── bbl
│   ├── bin
│   ├── include
│   ├── lib
│   ├── lib64
│   ├── libexec
│   ├── riscv64-unknown-linux-gnu
│   ├── share
│   ├── sysroot
│   └── vmlinux
├── README.md
└── software
    ├── app
    ├── buildroot
    ├── linux-kernel
    ├── pk
    ├── riscv-gnu-toolchain
    ├── sail
    └── spike

```

### Toolchain

This is the RISC-V C and C++ cross-compiler. It supports two build modes: a generic ELF/Newlib toolchain and a more sophisticated Linux-ELF/glibc toolchain. Installation guide refers to riscv-gnu-toolchain/README.md. 

The GCC version is 10.3.0

### Bootloader 

It uses BBL (Berkeley Boot Loader) as the bootloader to boot Linux.

### Linux Kernel

Based on Gnu Kernel Release Version 5.7

From (https://mirrors.edge.kernel.org/pub/linux/kernel/v5.x/linux-5.7.tar.gz)

### File system

Support ramdisk and busybox-1.33.1.

## Quick Startup

### Setup Environment

#### Install development kit

**Online installation**

```
yum install epel-release

yum install autoconf automake python3 libmpc-devel mpfr-devel gmp-devel gawk  bison flex texinfo patchutils gcc gcc-c++ zlib-devel expat-devel dtc git curl-devel expat-devel gettext-devel openssl-devel zlib-devel asciidoc perl-ExtUtils-MakeMaker openssl-devel bzip2-devel expat-devel gdbm-devel readline-devel sqlite-devel gcc gcc-c++ openssl-devel
```

**Offline installation**

Install all rpm files in the **yum.6.pkg.tar.gz**

#### Install compilation environment

Make sure you have root privileges.

The Following *wget packages* can be found in the **pkg.6.tar.gz**

**Install GMP**

wget https://gmplib.org/download/gmp/gmp-6.2.1.tar.xz

./configure --prefix=/usr/local/gmp

make && make install

**Install MPFR**

wget https://www.mpfr.org/mpfr-current/mpfr-4.1.0.tar.gz

./configure --prefix=/usr/local/mpfr --with-gmp=/usr/local/gmp

make && make install

**Install MPC**

wget [ftp://ftp.gnu.org/gnu/mpc/mpc-1.2.1.tar.gz](ftp://ftp.gnu.org/gnu/mpc/mpc-1.2.1.tar.gz)

./configure --prefix=/usr/local/mpc --with-gmp=/usr/local/gmp --with-mpfr=/usr/local/mpfr

make && make install

**Link GMP/MPFR/MPC**

ln -sf /usr/local/mpc/lib/libmpc.so /usr/lib64/libmpc.so
ln -sf /usr/local/mpc/lib/libmpc.so.3.2.1 /usr/lib64/libmpc.so.3
ln -sf /usr/local/mpfr/lib/libmpfr.so /usr/lib64/libmpfr.so
ln -sf /usr/local/mpfr/lib/libmpfr.so.6 /usr/lib64/libmpfr.so.6
ln -sf /usr/local/gmp/lib/libgmp.so /usr/lib64/libgmp.so
ln -sf /usr/local/gmp/lib/libgmp.so.10 /usr/lib64/libgmp.so.10

**Install GCC**

wget [ftp://ftp.gnu.org/gnu/gcc/gcc-7.5.0/gcc-7.5.0.tar.gz](ftp://ftp.gnu.org/gnu/gcc/gcc-7.5.0/gcc-7.5.0.tar.gz)

./configure --prefix=/usr/local/gcc --with-gmp=/usr/local/gmp --with-mpfr=/usr/local/mpfr --with-mpc=/usr/local/mpc --enable-bootstrap --enable-shared --enable-threads=posix --enable-checking=release --with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions --enable-gnu-unique-object --enable-linker-build-id --with-linker-hash-style=gnu --enable-languages=c,c++ --enable-plugin --enable-initfini-array --disable-libgcj --enable-gnu-indirect-function --with-tune=generic --disable-multilib

make && make install

**Link GCC**

ln -sf /usr/local/gcc/lib64/libstdc++.so.6 /usr/lib64/libstdc++.so.6
ln -sf /usr/local/gcc/bin/gcc /usr/bin/gcc
ln -sf /usr/local/gcc/bin/c++ /usr/bin/c++
ln -sf /usr/local/gcc/bin/g++ /usr/bin/g++

**Install Bison**

wget http://ftp.gnu.org/gnu/bison/bison-3.7.tar.xz
./configure
make && make install

**Upgrade Make to 4.3**

wget http://ftp.gnu.org/gnu/make/make-4.3.tar.gz
./configure
make && make install
ln -sf /usr/local/bin/make /usr/bin/gmake

**Install Python3.6 (Optional)**

wget https://www.python.org/ftp/python/3.6.5/Python-3.6.5.tgz
tar zxvf Python-3.6.5.tgz
cd Python-3.6.5
open *Modules/Setup.dist*
uncomment the following lines：
*209* SSL=/usr/local/ssl
*210* _ssl _ssl.c 
*211* -DUSE_SSL -I$(SSL)/include -I$(SSL)/include/openssl 
*212* -L$(SSL)/lib -lssl -lcrypto
./configure --prefix=/usr/local/python3
make && make install

**Install Git 2.33 (Optional)**

wget https://mirrors.edge.kernel.org/pub/software/scm/git/git-2.33.0.tar.gz
make prefix=/usr/local/git all
make prefix=/usr/local/git install

### Build SDK Components

#### **Build gcc**

```
$make gcc
```

The output is under the  $(pwd)/output/bin/ directory.

#### **Build buildroot**

```
$make brt
```

The output is under the  $(pwd)/output/bin/ directory.

#### **Build pk**

```
$make pk
```

The output is under the $(pwd)/output/riscv64-unknown-linux-gnu/bin/ directory.

#### **Build spike**

```
$make spike
```

The output is under the  $(pwd)/output/bin/ directory.

#### **Build bbl**

```
$make bbl
```

The output is under the $(pwd)/output/ directory.

#### Build test

```
$make app=dhrystone
```

### Run on Spike

#### Run app

```
$make run app=dhrystone
```

#### Boot bbl/linux

```
$./output/bin/spike ./output/bbl
```

The bbl will boot linux.

### Run on FPGA

#### Boot bbl/linux

TBD 
