# RRV64 Verification User manual

## 1 Unit Test

### 1.1 Overview

The unit test framework consists of a series of testbenches. These testbenches are designed to test certain modules (i.e. dut) in the core. Testbench generates a well-designed signal for dut and monitors the output of the dut to detect possible bugs in the dut.

In the orv64 code, we provide the unit test of the divider as a reference for designing other testbench. The architecture of Testbench is shown in the following figure.

<img src="file:///C:/Users/86180/AppData/Local/Packages/oice_16_974fa576_32c1d314_3ffb/AC/Temp/msohtmlclip1/01/clip_image002.gif" alt="img" style="zoom: 150%;" />

The testbench instantiates the divider of orv64 as DUT. The control logic is used to generate the corresponding signal according to the test case and monitor the calculation results of DUT.

In order to test the divider, we prepared two type of test cases: corner cases and random cases. Corner cases contains some known error prone situations. For example, division by zero, mixed division of positive and negative numbers, etc. Random cases are randomly generated division formulas used to test the correctness of the divider under normal circumstances

### 1.2 Build and Run

```
$ source buld/env.sh

$ cd hardware/soc/ut

$ make 

$ make vcs
```



## 2 Core Test

### 2.1 Overview

RRV64 verification framework is designed for testing the RRV64 code and find out if there is any bug. The framework consists of Core, Cosim program and Test driving script. The block diagram of the whole system is shown below.

![img](assets/RRV64_Verification_User_Manual/clip_image002.gif)

### 2.2 Components

#### 2.2.1 Core

The Core is written in SystemVerilog, so we need to use simulator to compile it and generate executable file so that we can run it. We support two simulator: VCS and Verilator for now. The name of the executable files they generate are simv and Vtestbench, respectively.

The core's code is in repo/rtl directory.

#### 2.2.2 Cosim Program

The cosim program used for compare the architectural state data (e.g. pc and registers) from the rrv64 core with those from a golden model (e.g. spike). The cosim program is written in C/C++. It communicates with core through DPI and spike through pipeline. After getting the output data of the two, it compare them and report a cosim error if inconsistency is found.

COSIM program gets data from RTL through DPI. In RTL, we specially insert code for COSIM. These codes will call the callback function provided by cosim program through DPI at the beginning of simulation, the completion of each instruction and the exit of simulation, and pass the data to be compared to cosim program.

The cosim program uses fork to create sub processes and run a debug-mode spike in the sub processes. At the same time, the input and output of spike are redirected to its parent process (i.e. the cosim program) through the pipe. The parent process operates spike and obtains the output of spike by sending commands through the pipe.

The cosim program’s code is in repo/tb/cosim directory.

The flow chart of the whole cosim process is shown in the figure below:

![img](assets/RRV64_Verification_User_Manual/clip_image004.gif)

#### 2.2.3 Test Script

The test script is the outmost block of the framework. It search elf files to run according to the options you input, after finish, it analyzed the output to determine whether the test pass or fail. If the program runs timeout, the script will terminiate it and record it as “timeout”. 

The test script is written in python and it’s at repo/tb/rrvtest.

#### 2.2.4 Build Up Flow

We think it is helpful for users to understand the composition and logical structure of the verification framework by showing how the entire verification framework is built. The entire framework is built as follows:

 

![img](assets/RRV64_Verification_User_Manual/clip_image006.gif)

 

### 2.3 Build and Run

The project file structure is as follows. The default testbench is under hardware/soc/tb.

```
├── build
│   ├── env.sh
│   ├── Makefile
│   └── README.md
├── doc
│   ├── assets
│   ├── README.md
│   ├── RRV64_Architecture.md
│   ├── RRV64_Implementation_Guide.md
│   ├── RRV64_Integration_Guide.md
│   ├── RRV64_Performance_Model_User_Guide.md
│   ├── RRV64_Processor_Databook.md
│   ├── RRV64_Sail_User_Guide.md
│   ├── RRV64_SDK_User_Guide.md
│   ├── RRV64_Verification_User_Manual.md
│   ├── Spike_User_Guide.md
│   └── Talon_User_Guide.md
├── hardware
│   ├── soc
│   └── tool
├── Makefile -> build/Makefile
├── output
│   ├── bbl
│   ├── bin
│   ├── include
│   ├── lib
│   ├── lib64
│   ├── libexec
│   ├── riscv64-unknown-linux-gnu
│   ├── share
│   ├── sysroot
│   └── vmlinux
├── README.md
└── software
    ├── app
    ├── buildroot
    ├── linux-kernel
    ├── pk
    ├── riscv-gnu-toolchain
    ├── sail
    └── spike
```

#### 2.3.1 Prerequisites

##### 2.3.1.1 Build GCC

```
$ make gcc
```

The output is under the  $(pwd)/output/bin/ directory.

![image-20210913173759935](assets/RRV64_Verification_User_Manual/image-20210913173759935.png)

##### 2.3.1.2  Build Spike

```
$ make spike
```

The output is under the  $(pwd)/output/bin/ directory.

The spike is a simulation model for risc-v processor core, used to simulate the execution of risc-v software code. In RTL simulation, it is often used as a golden reference model, and the output of spike model can be compared with the RTL results to verify the correctness of the RTL core function.

#### 2.3.2   VCS Simulation

##### 2.3.2.1  RTL Testbench Compilation

To compile RRV64 with VCS:

```
$ source build/env.sh

$ cd hardware/soc/tb

$ make vcs
```

After successful compilation, the simv executable will be generated in the current directory.

##### 2.3.2.2  Build and Run Test Cases

To build test cases such as ISA-Test, coremark, dhrystone and etc.

The software code for emulation execution needs to be pre-compiled with gcc. Execute the following command in the hardware/soc/tb directory:

```
$ cd hardware/soc/tb

$ make benchmarks
```

The elf file for each test program is generated in the test_program/benchmarks and test_program/isa directories after the compilation is completed. If the test program code is modified later, it needs to be recompiled.

![image-20210913174955547](assets/RRV64_Verification_User_Manual/image-20210913174955547.png)

![image-20210913175003109](assets/RRV64_Verification_User_Manual/image-20210913175003109.png)

After successfully compiling and building, you can run a single case:

```
$ cd hardware/soc/tb

$ ./rrvtest -e test_program/isa/rv64mi-p-ma_addr
```

-e means run a single elf file in silent mode, only the test results will be output. If you want to see all of the output, you can use -E option. 

The default is with cosim simulation, if you need to turn off cosim, add the -n parameter. After the simulation is finished, if you see the DONE PASS message, it means the use case was executed successfully.

![image-20210913175257734](assets/RRV64_Verification_User_Manual/image-20210913175257734.png)

#### 2.3.3 Generate talon case

In order for random case generation, use talon case generation in following steps:

```
$ cd hardware/tool/talon

$ python3 talon.py -x instructions.xlsx -o test.c -c config/rv64/random_all.talon

$ make
```

the disassemble text file is located in hardware/tool/talon as test.elf.dump:

![ls](C:\Users\86180\AppData\Roaming\Typora\typora-user-images\image-20210915192557341.png)

In this case, a random constrained case is generated for regression. For more information about Talon, please refer to Talon_User_Guide.

#### 2.3.4 Run regression

To run all of the ISA-Test:

```
$ cd hardware/soc/tb

$ ./rrvtest -r
```

-r means run a single elf file in silent mode, only the test results will be output. If you want to see all of the output, you can use -R option.



